<?php

namespace App\RoomPlans;

use App\AbstractClasses\DeviceMethods;
use App\AbstractClasses\DeviceState;
use App\AbstractClasses\LightSensor;
use App\AbstractClasses\OnkyoMethods;
use App\DataObjects\LivingRoomDataObject;
use App\EnumClasses\DomoticzDevicesEnum;
use App\Models\DeviceLog;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;
use JsonException;

class LivingRoomPlan
{
//    use DomoticzMethodsTrait;
//    use SunsetSunriseTrait;
//    use DeviceMethodsTrait;

    private $idx;
    private $value;

    private $sunrise;
    private $sunset;
//    private $domoticzUrl;
    private $now;

    private $switchedDevices = [];
    /**
     * @var DeviceLog|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    private $doorSensor = null;
    /**
     * @var OnkyoMethods
     */
    private $onkyoMethods;
    /**
     * @var array
     */
    private $triggeredMethods;
    /**
     * @var Builder[]|Collection
     */
    private $deviceLogs;
    /**
     * @var DeviceState
     */
    private $deviceState;
    private $isHomeState = null;
    /**
     * @var DeviceMethods
     */
    private $deviceMethods;


    public function __construct(LivingRoomDataObject $livingRoomDataObject, DeviceMethods $deviceMethods)
    {
//        $this->domoticzUrl = config('domoticz.url');
        $this->idx = $livingRoomDataObject->idx;
        $this->value = $livingRoomDataObject->value;
        $this->sunrise = $livingRoomDataObject->sunrise;
        $this->sunset = $livingRoomDataObject->sunset;
        $this->onkyoMethods = $livingRoomDataObject->onkyoMethods;
        $this->now = Carbon::now()->format('H:i');
        $this->deviceLogs = $livingRoomDataObject->deviceLogs;
        $this->deviceMethods = $deviceMethods;

        $this->deviceLogs->where('idx', 0)->each(function ($value) {
            $time = now()->setTime(5, 0);
            $this->triggeredMethods[$value->state] = now()->lessThan($time) || $value->created_at->greaterThanOrEqualTo($time);
        });

        $this->deviceState = $livingRoomDataObject->deviceState;
    }

    /**
     * @param array|string $sensors
     * @return bool
     */
    public function motionOn($sensors): bool
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'] && $this->value === 'On') &&
            ($this->deviceMethods->isBetweenSunsetAndSunrise() || $this->deviceMethods->isTwilight())) {
            Log::notice('Motion on');
            $this->deviceMethods->switchAction($sensors, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $sensors
     * @return bool
     */
    public function doorOpenOn($sensors): bool
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['DOOR_SENSOR_LIVING_ROOM'] && $this->value === 'Open') && $this->deviceMethods->isBetweenSunsetAndSunrise()) {
            Log::notice('Door open on');
            $this->deviceMethods->switchAction($sensors, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $sensors
     * @return bool
     */
    public function manualOn($sensors): bool
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['SWITCH_LIVING_ROOM'] && $this->value === 'B3')) {
            Log::notice('Manual on second');
            $this->deviceMethods->switchAction($sensors, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $sensors
     * @return bool
     */
    public function manualToggle($sensors): bool
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['SWITCH_LIVING_ROOM'] && $this->value === 'B1')) {
            if (is_array($sensors)) {
                Log::notice('Manual Toggle array');
                foreach ($sensors as $sensor) {
                    $this->toggle($sensor);
                }
            } else {
                Log::notice('Manual Toggle');
                $this->toggle($sensors);
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $idx
     */
    private function toggle(int $idx): void
    {
        if ($this->deviceState->getCurrentState($idx) === 'Off') {
            $this->deviceMethods->switchAction($idx, 'On');
        } else {
            $this->deviceMethods->switchAction($idx, 'Off');
        }
    }

    /**
     * @param array|string $sensors
     * @return bool
     */
    public function manualOff($sensors): bool
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['SWITCH_LIVING_ROOM'] && $this->value === 'B2')) {
            Log::notice('Manual off');
            $this->deviceMethods->switchAction($sensors, 'Off');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @return bool
     * @throws JsonException
     */
    public function offAtSunrise($devices): bool
    {
        $secondAttempt = $this->sunrise->clone();
        $secondAttempt->setTime(9, 00);

        if (null === $this->idx &&
            $this->now >= $this->sunrise->format('H:i') &&
            $this->now <= $secondAttempt->format('H:i') &&
            (false === $this->deviceMethods->isTwilight() ||
                false === $this->isHome())
        ) {
            Log::notice('Off at sunrise');
            $this->deviceMethods->switchAction($devices, 'Off');
            return true;
        }

        return false;
    }

    /**
     * @param $devices
     * @return bool
     */
    public function offWhenLight($devices): bool
    {
        if ($this->idx !== null) {
            return false;
        }
        $livingRoomSensor = new LightSensor(DomoticzDevicesEnum::DEVICES['LUX_LIVING_ROOM'], $this->deviceLogs);

        if (false === $this->hasBeenSwitchedOn($devices, 60) &&
            true === $livingRoomSensor->lighterThen(17)->within(60)->check()
        ) {
            Log::notice('Off when light');
            $this->deviceMethods->switchAction($devices, 'Off');
            return true;
        }

        return false;
    }

    public function onWhenDark($devices): bool
    {
        if ($this->idx !== null ||
             false === now()->between(Carbon::parse('04:00'), Carbon::parse('22:00'))
        ) {
            return false;
        }

        $livingRoomSensor = new LightSensor(DomoticzDevicesEnum::DEVICES['LUX_LIVING_ROOM'], $this->deviceLogs);
        if (true === $this->isHome() &&
            false === $this->hasBeenSwitchedOff($devices, 60) &&
            true === $livingRoomSensor->darkerThen(17)->within(60)->check()
        ) {
            Log::notice('On when dark');

            $this->deviceMethods->switchAction($devices, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @param int|null $addMinutes
     * @return bool
     */
    public function onAtSunset($devices, ?int $addMinutes = null): bool
    {
        $sunset = $this->sunset->clone();
        null === $addMinutes ?: $sunset->addMinutes($addMinutes);

        if ($this->now === $sunset->format('H:i')) {
            Log::notice('On at sunset');
            $this->deviceMethods->switchAction($devices, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @return bool
     */
    public function offAtMidnight($devices): bool
    {
        $midnight = '01:00';
        if ($this->now === $midnight) {
            Log::notice('Off at midnight');
            $this->deviceMethods->switchAction($devices, 'Off');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @return bool
     */
    public function onOnce($devices): bool
    {
        if (
            ($this->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'] && $this->value === 'On') &&
            false === $this->isTriggeredToday(__FUNCTION__) &&
            false === $this->hasBeenSwitchedOff($devices)
        ) {
            Log::notice('On once');
            $this->deviceMethods->switchAction($devices, 'On');

            $this->setIsTriggeredToday(__FUNCTION__);
            return true;
        }

        return false;
    }


    /**
     * @param array|string $devices
     * @return bool
     */
    public function onOnceAfterSunsetWhenHome($devices): bool
    {
        $earlyAttempt = $this->sunset->clone();
        $earlyAttempt->subMinutes(60);
        if ($this->now >= $earlyAttempt->format('H:i') &&
            false === $this->isTriggeredToday(__FUNCTION__) &&
            true === $this->isHome() &&
            false === $this->hasBeenSwitchedOff($devices) &&
            true === $this->deviceMethods->isTwilight()
        ) {
            Log::notice('On after early sunset when home');
            $this->deviceMethods->switchAction($devices, 'On');

            $this->setIsTriggeredToday(__FUNCTION__);
            return true;
        }

        if ($this->now >= $this->sunset->format('H:i') &&
            false === $this->isTriggeredToday(__FUNCTION__) &&
            true === $this->isHome() &&
            false === $this->hasBeenSwitchedOff($devices)
        ) {
            Log::notice('On after sunset when home');
            $this->deviceMethods->switchAction($devices, 'On');

            $this->setIsTriggeredToday(__FUNCTION__);
            return true;
        }

        return false;
    }

    private function isTriggeredToday(string $methodName): bool
    {
        return $this->triggeredMethods[$methodName] ?? false;
    }

    private function setIsTriggeredToday(string $methodName): bool
    {
        $this->triggeredMethods[$methodName] = true;

        $deviceLog = new DeviceLog();
        $deviceLog->idx = 0;
        $deviceLog->state = $methodName;
        $deviceLog->save();

        return false;
    }


    /**
     * @param array|string $devices
     * @param bool|null $useTwilight
     * @return bool
     */
    public function motionOnWithManualOverride($devices, ?bool $useTwilight = true): bool
    {
//        $start = now()->setTime(05, 00, 00);
//        $start = $start->format('y-m-d H:i:s');
//        $end = $this->sunrise->clone();
//        $end = $end->addMinutes(60)->format('y-m-d H:i:s');
//        $now = now()->format('y-m-d H:i:s');

        $livingRoomSensor = new LightSensor(DomoticzDevicesEnum::DEVICES['LUX_LIVING_ROOM'], $this->deviceLogs);
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'] && $this->value === 'On') &&
//            $now > $start && $now < $end &&
//            (!$useTwilight || $this->isTwilight()) &&
            false === $this->hasBeenOffForMinutes($devices, 120) &&
            true === $livingRoomSensor->darkerThen(17)->within(60)->check() &&
            false === $this->isTriggeredToday(__FUNCTION__)
        ) {
            $this->setIsTriggeredToday(__FUNCTION__);

            Log::notice('Motion on with manual override');
            $this->deviceMethods->switchAction($devices, 'On');
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @return bool
     */
    public function offAtTwelveWhenNotHome($devices): bool
    {
        if ($this->now === '23:59' && false === $this->isHome()) {
            Log::notice('offAtTwelveWhenNotHome');
            $this->deviceMethods->switchAction($devices, 'Off');
            return true;
        }

        return false;
    }

    /**
     * @throws JsonException
     */
    public function dimWhenTvOn($devices, int $dimLevel): bool
    {
        //16 is BD/DVD, 18 is TV

        if (null === $this->idx &&
            $this->now > '19:15' &&
            false === $this->isTriggeredToday(__FUNCTION__)
        ) {
            $onkyoPreset = $this->onkyoMethods->getInputSource();
            if (true === $this->onkyoMethods->onkyoIsOn() && ($onkyoPreset === 18 || $onkyoPreset === 16)) {
                Log::info('Dim when TV is on');
                $this->dimAction($devices, $dimLevel);

                $this->setIsTriggeredToday(__FUNCTION__);
                return true;
            }
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @param int $dimLevel
     * @return bool
     * @throws JsonException
     */
    public function dimScheduleEvening($devices, int $dimLevel): bool
    {
        if (null === $this->idx && $this->now === '19:15') {
            Log::notice('Dim schedule evening executed');
            $this->dimAction($devices, $dimLevel);
            return true;
        }

        return false;
    }

    /**
     * @param array|string $devices
     * @param int $dimLevel
     * @return bool
     * @throws JsonException
     */
    public function dimScheduleMorning($devices, int $dimLevel): bool
    {
        if (null === $this->idx && $this->now === '05:00') {
            Log::notice('Dim schedule morning executed');
            $this->dimAction($devices, $dimLevel);
            return true;
        }

        return false;
    }

    /**
     * @return void
     */
    public function leavingHome(): void
    {
        if (null !== $this->idx) {
            return;
        }

        if (null === $this->doorSensor) {
            /** @var DeviceLog $doorSensor */
            $this->doorSensor = $this->deviceLogs
                ->filter(function (DeviceLog $log) {
                    return $log->idx === DomoticzDevicesEnum::SWITCHES['DOOR_SENSOR_LIVING_ROOM']
                        && $log->state === 'Open';
                })
                ->sortByDesc('created_at')
                ->first();

        }

        if (null === $this->doorSensor) {
            return;
        }


        if (false === $this->hasMovementBeforeBackdoorCloses() ||
            true === $this->motionSensorTurnedOffOneMinuteAfterBackdoorCloses() ||
            false === $this->hasActivityInLastFiveMinutes()
        ) {
            return;
        }

        $this->deviceMethods->switchAction(DomoticzDevicesEnum::DEVICES['ALARM'], 'On');
    }

    /**
     * @return void
     */
    public function enterHome(): void
    {
        //todo dit script moet weg.Dit zou je met een code moeten doen.
        if ($this->idx === DomoticzDevicesEnum::SWITCHES['DOOR_SENSOR_LIVING_ROOM'] &&
            $this->value === 'Open' &&
            false === $this->hasActivityInLastFiveMinutes()
        ) {
            $this->deviceMethods->switchAction(DomoticzDevicesEnum::DEVICES['ALARM'], 'Off');
        }
    }


    /**
     * @param array|string $sensors
     * @param int|null $withinMinutes Optional parameter to filter logs based on minutes ago.
     * @return bool
     */
    public function hasBeenSwitchedOff($sensors, ?int $withinMinutes = null): bool
    {
        $sensors = is_array($sensors) ? $sensors : [$sensors];

        $hasOnState = false;
        $logs = $this->deviceLogs->sortBy('created_at')
            ->filter(function (DeviceLog $log) use ($sensors, $withinMinutes, &$hasOnState) {
                $isInSensors = in_array($log->idx, $sensors);
                $isOffState = $log->state === 'Off';
                $hasOnState = !($hasOnState === false) || $log->state === 'On';

                if ($withinMinutes !== null) {
                    $timeLimit = now()->subMinutes($withinMinutes);
                    $isWithinMinutes = $log->created_at->greaterThanOrEqualTo($timeLimit);
                } else {
                    $isWithinMinutes = $log->created_at->greaterThanOrEqualTo($this->sunset) ||
                        (
                            $log->created_at->greaterThanOrEqualTo($this->sunrise) &&
                            $log->created_at->format('Y-m-d H:i:s') < $this->sunrise->format('Y-m-d') . '10:00:00'
                        );
                }

                return $isInSensors && $hasOnState && $isOffState && $isWithinMinutes;
            })
            ->count();
        dump($this->deviceLogs->pluck('idx', 'state')->toArray());

        return $logs > 0;
    }

    private function hasBeenOffForMinutes($sensors, int $minutes): bool
    {
        $sensors = is_array($sensors) ? $sensors : [$sensors];

        $logs = $this->deviceLogs->sortBy('created_at')
            ->filter(function (DeviceLog $log) use ($sensors, $minutes) {
                $isInSensors = in_array($log->idx, $sensors);
                $isOnState = $log->state === 'On';

                $timeLimit = now()->subMinutes($minutes);
                $isWithinMinutes = $log->created_at->greaterThanOrEqualTo($timeLimit);

                return $isInSensors && $isOnState && $isWithinMinutes;
            })
            ->count();

        return $logs === 0;
    }

    private function hasBeenSwitchedOn($sensors, ?int $withinMinutes = null): bool
    {
        $sensors = is_array($sensors) ? $sensors : [$sensors];

//        dump($this->deviceLogs->pluck('idx', 'state'));
        $logs = $this->deviceLogs
            ->filter(function (DeviceLog $log) use ($sensors, $withinMinutes) {
                $isInSensors = in_array($log->idx, $sensors);
                $isOnState = $log->state === 'On';

                if ($withinMinutes !== null) {
                    $timeLimit = now()->subMinutes($withinMinutes);
                    $isWithinMinutes = $log->created_at->greaterThanOrEqualTo($timeLimit);
                } else {
                    $isWithinMinutes = $log->created_at->greaterThanOrEqualTo($this->sunset) ||
                        (
                            $log->created_at->greaterThanOrEqualTo($this->sunrise) &&
                            $log->created_at->format('Y-m-d H:i:s') < $this->sunrise->format('Y-m-d') . '10:00:00'
                        );
                }

                return $isInSensors && $isOnState && $isWithinMinutes;
            })
            ->count();

        return $logs > 0;
    }


    private function hasMovementBeforeBackdoorCloses(): bool
    {
        /** @var DeviceLog $motionSensor */
        $motionSensor = $this->deviceLogs
            ->filter(function (DeviceLog $log) {
                return $log->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM']
                    && $log->state === 'On';
            })
            ->sortByDesc('created_at')
            ->first();


        return true === $this->doorSensor->created_at->gt($motionSensor->created_at);
    }

    private function motionSensorTurnedOffOneMinuteAfterBackdoorCloses(): bool
    {
        // Clone the created_at value to ensure immutability.
        $endTime = $this->doorSensor->created_at->copy();

        /** @var DeviceLog $motionSensor */
        $count = $this->deviceLogs
            ->filter(function (DeviceLog $log) use ($endTime) {
                // Ensure we are comparing Carbon instances
                return $log->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'] &&
                    $log->state === 'Off' &&
                    $log->created_at->greaterThan($this->doorSensor->created_at) &&
                    $log->created_at->lessThan($endTime->addMinute());
            })
            ->count();

        return $count === 1;
    }


    private function hasActivityInLastFiveMinutes(): bool
    {
        $activityCount = $this->deviceLogs
            ->filter(function ($log) {
                return in_array($log->idx, [
                    DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM']
                ]);
            })
            ->count();


        return $activityCount > 0;
    }

    /**
     * @param array|string $devices
     * @param string $state
     * @return void
     * @throws JsonException
     */


    /**
     * @param array|string $devices
     * @param string $dimLevel
     * @return void
     * @throws JsonException
     */
    private function dimAction($devices, string $dimLevel): void
    {
        if (is_array($devices)) {
            Log::info('Dim: is array');
            foreach ($devices as $device) {
                if ($this->deviceState->getCurrentState($device) === 'Off') {
                    Log::info('dimAction while off, with array of devices');
                    if (in_array($device, $this->switchedDevices)) {
                        sleep(1);
                    }
                    $this->deviceMethods->setDimLevel($device, $dimLevel);
                    $this->deviceMethods->enableDisableSwitch($device, 'Off');
                    $this->switchedDevices[] = $device;
                    return;
                }
                Log::info(sprintf('Dim with array is not off. device id: %s', $device));
                if (in_array($device, $this->switchedDevices)) {
                    sleep(1);
                }
                $this->deviceMethods->setDimLevel($device, $dimLevel);
                $this->switchedDevices[] = $device;
            }
        } else {
            Log::info('Dim: is not array');
            if ($this->deviceState->getCurrentState($devices) === 'Off') {
                Log::info('dimAction while off');
                if (in_array($devices, $this->switchedDevices)) {
                    sleep(1);
                }
                $this->deviceMethods->setDimLevel($devices, $dimLevel);
                $this->deviceMethods->enableDisableSwitch($devices, 'Off');
                $this->switchedDevices[] = $devices;
                return;
            }
            Log::info(sprintf('Dim: while On. devices: %s dimLevel: %s', $devices, $dimLevel));
            if (in_array($devices, $this->switchedDevices)) {
                sleep(1);
            }
            $this->deviceMethods->setDimLevel($devices, $dimLevel);
            $this->switchedDevices[] = $devices;
        }
    }


    /**
     * @return bool
     */
    public function isHome(): bool
    {
        if ($this->deviceState->getCurrentState(DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM']) === 'On') {
            return true;
        }
        if (null !== $this->isHomeState) {
            return $this->isHomeState;
        }

        $interval = Carbon::now()->subMinutes(30);

        $maySwitchOn = $this->deviceLogs
            ->filter(function ($log) use ($interval) {
                return $log->idx === DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'] &&
                    $log->created_at instanceof Carbon &&
                    $log->created_at->greaterThanOrEqualTo($interval);
            })
            ->count();

        $this->isHomeState = $maySwitchOn >= 1;
        return $maySwitchOn >= 1;
    }

}
