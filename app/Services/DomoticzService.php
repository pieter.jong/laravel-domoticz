<?php

namespace App\Services;

use JsonException;

class DomoticzService
{

    /**
     * @var \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $domoticzUrl;
    private $userName;
    private $password;

    public function __construct()
    {
        $this->domoticzUrl = config('domoticz.url');
        $this->userName = config('domoticz.username');
        $this->password = config('domoticz.password');
    }

    /**
     * @throws JsonException
     */
    public function enableDisableSwitch(int $idx, string $cmd): string
    {
        $ch = $this->setupCurl();

        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=command&param=switchlight&idx=%s&switchcmd=%s', $this->domoticzUrl, $idx, $cmd));
        curl_exec($ch);
        $reply = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);

        return $reply['status'] === 'OK' ? 'OK' : $reply = 'ERROR';
    }

    /**
     * @throws JsonException
     */
    private function enableDisableScene($idx, $cmd)
    {
        //todo should be transformed to a curl request.
        $reply = json_decode(
            file_get_contents($this->domoticzUrl . 'type=command&param=switchscene&idx=' . $idx . '&switchcmd=' . $cmd),
            true, 512, JSON_THROW_ON_ERROR
        );
        return $reply['status'] === 'OK' ? 'OK' : 'ERROR';
    }

    /**
     * @throws JsonException
     */
    public function setDimLevel($idx, $level): string
    {
        if ($level > 0 && $level < 100) {
            ++$level;
        }
        $ch = $this->setupCurl();

        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=command&param=switchlight&idx=%s&switchcmd=Set%%20Level&level=%s',$this->domoticzUrl, $idx, $level));
        $reply = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);

        return $reply['status'] === 'OK' ? 'OK' : 'ERROR';
    }



    /**
     * @throws JsonException
     */
    private function getCurrentState($idx)
    {
        $ch = $this->setupCurl();
        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=devices&rid=%d', $this->domoticzUrl, $idx));

        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
        if (isset($content['result'])) {
            foreach ($content['result'] as $dom) {
                return $dom['Status'] ?? null;
            }
        }
        curl_close($ch);

        return null;
    }

    public function getAllDeviceStates(): array
    {
        $ch = $this->setupCurl();
        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=command&param=getdevices', $this->domoticzUrl));
        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
        curl_close($ch);

        if (isset($content['result'])) {
            $devices = [];
            foreach ($content['result'] as $device) {
                $devices[$device['idx']] = $device['Status'] ?? null; // Store device status by idx
            }
            return $devices;
        }

        return [];
    }

//    private function getAllDeviceStates(): array
//    {
//        $ch = $this->setupCurl();
//        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=getdevices', $this->domoticzUrl));
//
//        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
//        curl_close($ch);
//
//        if (isset($content['result'])) {
//            $devices = [];
//            foreach ($content['result'] as $device) {
//                $devices[$device['idx']] = $device['Status'] ?? null; // Store device status by idx
//            }
//            return $devices;
//        }
//
//        return [];
//    }



    private function getTemperature($idx)
    {
        $ch = $this->setupCurl();
        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=devices&rid=%d', $this->domoticzUrl, $idx));

        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
        curl_close($ch);

        return $content['result'][0]['Temp'] ?? null;
    }

    private function getHistory($idx)
    {
        $ch = $this->setupCurl();
//        curl_setopt($ch, CURLOPT_URL, sprintf('%stype=lightlog&idx=%s', $this->domoticzUrl, $idx));
        curl_setopt($ch, CURLOPT_URL, sprintf('%stype=graph&sensor=Percentage&idx=%s&range=day', $this->domoticzUrl, $idx));
        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
        if ($content) {
            foreach ($content['result'] as $dom) {
                dd(count($content['result']));
                dump($dom);
                return $dom['Status'] ?? null;
            }
        }
        curl_close($ch);

        return null;
    }

    public function setupCurl()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt_array($ch, array(
            CURLOPT_HTTPHEADER => array(
                'Authorization: basic ' . base64_encode($this->userName.':'.$this->password),
            ),
        ));

        return $ch;
    }
}
