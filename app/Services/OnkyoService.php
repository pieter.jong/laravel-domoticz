<?php


namespace App\Services;


use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class OnkyoService
{
    /**
     * A list of eicp commands for onkyo can be found at this url
     * https://github.com/miracle2k/onkyo-eiscp/blob/master/eiscp-commands.yaml
     */

    public const SOUND_MODES = [
        'Dolby surround' => "!1LMD80",
        'Music' => "!1LMD82",
        'Stereo' => "!1LMD00",
        'All ch Stereo' => "!1LMD0C",
    ];

    public const SOURCES = [
        'Net' => 'Net',
        'Spotify' => 'Spotify',
        'Chromecast' => 'Chromecast',
        'Tv' => 'Tv',
    ];

    public const PRESETS = [
        'Qmusic' => '01',
        '3fm' => '02',
        'Lounge fm' => '03',
        'Freeze fm' => '04',
        'Sky radio' => '05',
        'Veronica' => '06',
        'NPO radio 2' => '07',
        'Slam fm' => '08',
        'Kink' => '09',
    ];

    private $ip = '192.168.178.166';
    private $port = 60128;
    private $max_volume = 50;
    private $debug = false;
    /**
     * @var false|resource
     */
    private $fp;

    public function setOnkyoSocket()
    {
        if (null === cache('onkyo')) {
            try {
                if (!is_resource($this->fp)) {
                    $this->fp = stream_socket_client(
                        "tcp://" . $this->ip . ":" . $this->port,
                        $errno,
                        $errstr,
                        3
                    );

                    if (!$this->fp) {
                        throw new \Exception("Connection failed: $errstr ($errno)");
                    }
                }
            } catch (\Exception $e) {
                $this->fp = null;
                Log::error("Onkyo connection error: " . $e->getMessage());
                Cache::add('onkyo', false, 60); // Mark Onkyo as unreachable for 60 seconds
            }
        } else {
            Log::info("Onkyo is currently marked as unreachable in cache.");
        }
    }


    public function turnOn(): OnkyoService
    {
        $task = $this->send_cmd($this->concatCommand('!1PWR', '01'));
        sleep(1);
        return $this;
    }

    public function turnOff(): OnkyoService
    {
        $task = $this->send_cmd($this->concatCommand('!1PWR', '00'));
        return $this;
    }

    public function setSource(string $string): OnkyoService
    {
        switch (true) {
            case $string === 'Chromecast':
                $response = $this->send_cmd($this->concatCommand('!1SLI', '11'));
                break;
            case $string === 'Tv':
                $response = $this->send_cmd($this->concatCommand('!1SLI', '12'));
                break;
            case $string === 'Net':
                $response = $this->send_cmd($this->concatCommand('!1SLI', '2B'));
                break;
            case $string === 'Spotify':
                $response = $this->send_cmd($this->concatCommand('!1NSV', '0A'));
                sleep(10);
                $this->play();
                break;
        }
        sleep(1);

        return $this;
    }

    public function setRadioPreset(string $preset): OnkyoService
    {
//        $response = $this->send_cmd($this->concatCommand('!1NSV', '01'));
//        sleep(5);
        $response = $this->send_cmd($this->concatCommand('!1NPR', $preset));
        return $this;
    }

    public function setMainVolume(int $volume): OnkyoService
    {
        $task = $this->send_cmd($this->concatCommand('!1MVL', $volume));
        sleep(1);
        return $this;
    }

    public function play(): OnkyoService
    {
        $task = $this->send_cmd($this->concatCommand('!1NTC', 'PLAY'));
        return $this;
    }

    public function setCenterSpeakerVolume(int $volume)
    {
        $volume = "!1CTL+" . $volume;
        $this->send_cmd($volume);

        //todo ff testen welke werkt.

//        $volume = $volume > 12 ? 12 : $volume;
//        $volume = strtoupper(dechex($volume));
//        $var = "!1CTL+" . $volume;
//        $task = $this->send_cmd($var);
    }

    /**
     * @throws \Exception
     */
    public function setSoundMode(string $modeName): OnkyoService
    {
        if (!isset(self::SOUND_MODES[$modeName])) {
            throw new \Exception(sprintf('Sound mode "%s" does not exist.', $modeName));
        }

        $this->send_cmd(self::SOUND_MODES[$modeName]);
        sleep(1);

        return $this;
    }

    public function onkyoIsOn(): bool
    {
        $response = $this->get_status("!1PWRQSTN");
        $response = hexdec(str_replace('!1PWR', '', $response));

        return $response === 1;
    }

    public function getRadioPreset()
    {
        //This method does not work
//        $response = $this->send_cmd($this->concatCommand('!1NSV', '01'));
//        $response = $this->send_cmd($this->concatCommand('!1NPR', $preset));
        $response = $this->get_status("!1NTRQSTN");
        return $response;
        return hexdec(str_replace('!NAL', '', $response));
    }

    public function getMainVolume()
    {
        $response = $this->get_status("!1MVLQSTN");
        return hexdec(str_replace('!1MVL', '', $response));
    }

    public function getInputSource()
    {
        $response = $this->get_status("!1SLIQSTN");
        return hexdec(str_replace('!1SLI', '', $response));
        //16 is BD/DVD
        //18 is TV
    }

    private function get_status($cmd)
    {
        do {
            $this->send_cmd($cmd, false);
            $status = fread($this->fp, 80);
            $status = substr($status, strpos($status, "!"));
            $status = substr($status, 0, strlen($status) - 3);
            if ($this->debug) {
                echo "\n*** get_status:" . $cmd . " : " . $status;
            }
        } while (substr_compare($status, "!1NLS", 0, 5) == 0);
        fclose($this->fp);
        return $status;
    }


    private function send_cmd(string $cmd, $closeSocket = true): string
    {
        if (!is_resource($this->fp)) {
            $this->setOnkyoSocket();
        }

        $length = strlen($cmd);
        ++$length;
        $code = chr($length);
        $line = "ISCP\x00\x00\x00\x10\x00\x00\x00$code\x01\x00\x00\x00" . $cmd . "\x0D";
        if (true === $this->debug) {
            echo "\n*** send_cmd:" . $line;
        }

        fwrite($this->fp, $line);
        if (true === $closeSocket) {
            fclose($this->fp);
        }

        return $line;
    }

    private function concatCommand(string $type, string $value): string
    {

        switch (true) {
            case $type === '!1TUN':
            case $type === '!1AMT':
            case $type === '!1SLI':
            case $type === '!1PWR':
            case $type === '!1NTC':
            case $type === '!1NSV':
                $message = $type . $value;
                break;
            case $type === '!1MVL':
                $volume = min((int)$value, $this->max_volume);
                $volume = strtoupper(dechex($volume));
                if (strlen($volume) == '1') {
                    $volume = "0" . $volume;
                }
                $message = "!1MVL" . $volume;
                break;
            case $type === '!1PRS':
            case $type === '!1PRM':
            case $type === '!1NPR':
//                dump('joe');
//                $message = $type . dechex($value);
                $message = $type . $value;
//                $message = '9.6520637438034E+27';
                break;
            default:
                $message = $type . $value;
//                $message = $type . strtoupper(str_pad(dechex($value), 2, '0', STR_PAD_LEFT));
        }

        return $message;
    }


}
