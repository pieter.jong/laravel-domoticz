<?php

namespace App\EnumClasses;

class DomoticzDevicesEnum
{
    public const DEVICES = [
        'LIGHT_HALLWAY' => 359,
        'LIGHT_LIVING_ROOM' => 378,
        'WCD_WOODEN_LAMP' => 360,
        'WCD_WOODEN_CLOSET' => 361,
        'WCD_CHRISTMAS_TREE' => 329,//
        'WCD_CHRISTMAS_OUTSIDE' => 330,//
        'WCD_PLANT_LAMP' => 383,//
        'ALARM' => 331,
        'LIGHT_OUTDOOR_1' => 372,
        'LIGHT_OUTDOOR_2' => 373,
        'WCD_HEAT_MAT' => 352,
        'TEMPERATURE_SENSOR' => 327,//
        'TEMPERATURE_SHOWER' => 368,
        'HUMIDITY_SHOWER' => 367,
        'DUCO' => 111, //
        'LUX_LIVING_ROOM' => 369,
        'LUX_HALLWAY' => 381,
    ];

    public const SWITCHES = [
        'MOTION_HALLWAY' => 382,
        'MOTION_LIVING_ROOM' => 370,
        'SWITCH_LIVING_ROOM' => 364,
        'DOOR_SENSOR_LIVING_ROOM' => 326,
        'RADIO_STATION_SELECT' => 380,
    ];
}
