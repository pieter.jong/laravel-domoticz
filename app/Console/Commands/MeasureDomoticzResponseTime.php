<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Exception;

class MeasureDomoticzResponseTime extends Command
{
//    use DomoticzMethodsTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'domoticz:measure-response';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Measures the response time of the Domoticz server';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

//        $this->domoticzUrl = config('domoticz.url');
//        try {
//            $startTime = microtime(true);
//
//            $this->getAllDeviceStates();
//
//            $endTime = microtime(true);
//            $responseTime = $endTime - $startTime;
//
//            $this->info("Response Time: " . number_format($responseTime, 2) . " seconds");
//
//        } catch (Exception $e) {
//            $this->error("An error occurred: " . $e->getMessage());
//        }
//
//        return 0;
    }
}
