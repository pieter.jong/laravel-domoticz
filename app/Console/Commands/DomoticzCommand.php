<?php

namespace App\Console\Commands;

use App\AbstractClasses\AutomationVariables;
use App\AbstractClasses\DeviceMethods;
use App\AbstractClasses\DeviceState;
use App\AbstractClasses\OnkyoMethods;
use App\DataObjects\LivingRoomDataObject;
use App\EnumClasses\DomoticzDevicesEnum;
use App\Models\DeviceLog;
use App\RoomPlans\LivingRoomPlan;
use App\Services\OnkyoService;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\Log;
use JsonException;

class DomoticzCommand extends Command
{
//    use DomoticzMethodsTrait;
//    use DeviceMethodsTrait;

    private $domoticzUrl = '';
    private $sunset;
    private $sunrise;
    private $idx;
    private $value;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:domoticz {idx?} {value?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Within this command you can configure you whole home automation, starting with the handle method';
    /**
     * @var LivingRoomPlan
     */
    private $livingRoomPlan;
    /**
     * @var OnkyoService
     */
    private $onkyoService;
    /**
     * @var DeviceState
     */
    private $deviceState;
    /**
     * @var \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private $deviceLogs;
    /**
     * @var array
     */
    private $switchedDevices;
    /**
     * @var OnkyoMethods
     */
    private $onkyoMethods;
    /**
     * @var DeviceMethods
     */
    private $deviceMethods;

    /**
     * Create a new command instance.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     * @throws JsonException
     */
    public function handle(): int
    {
        $this->idx = $this->argument('idx') ? (int)$this->argument('idx') : null;
        $this->value = $this->argument('value') ? (string)$this->argument('value') : null;
        Log::notice(sprintf('Command call with: idx: %s value: %s', $this->idx, $this->value));



        // todo dit moet een object worden en mee worden gegeven aan livingroomDataObject
        //  Of DeviceMethodsTrait moet een object worden.

        $automationVariables = new AutomationVariables($this->idx, $this->value);
        $this->onkyoMethods = new OnkyoMethods($automationVariables->deviceState);
        $this->deviceMethods = new DeviceMethods($automationVariables);
        $livingRoomDataObject = new LivingRoomDataObject($automationVariables, $this->onkyoMethods);
        $this->sunset = $automationVariables->sunset;
        $this->sunrise = $automationVariables->sunrise;
        $this->deviceState = $automationVariables->deviceState;


        if (false === $this->hasRelevantDevices()) {
            Log::notice('No relevant device');
            return 0;
        }

        $this->livingRoomPlan = new LivingRoomPlan($livingRoomDataObject, $this->deviceMethods);


        $this->storeState();

        $this->livingRoomPlan->leavingHome();
        $this->livingRoomPlan->enterHome();

        $this->hallwayScripts();
        $this->livingRoomScripts();
        $this->outsideScripts();
//        $this->christmasScripts();
//        $this->heatMatScript();
        $this->showerScript();

        $this->deviceState->StoreCurrentStateStates();
        Log::info(sprintf('isHome = %s', $this->livingRoomPlan->isHome()));

        return 0;
    }

    /**
     * @throws JsonException
     * @throws Exception
     */
    private function livingRoomScripts(): void
    {
        $this->livingRoomPlan->dimWhenTvOn(DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'], 2);
        $this->livingRoomPlan->dimScheduleEvening(DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'], 10);
        $this->livingRoomPlan->dimScheduleMorning(DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'], 40);

        $this->onkyoMethods->changeRadioPreset($this->idx);

        //Todo ik moet nog eens goed nadenken of het uitdoen van de lampen wanneer we boven aan het werk zijn. Dat kan mischien met swithcOffWithInterval.

        //Todo deze gaat niet gelijk aan wanneer je binnen komt omdat er nog geen LUX data bekend is.
        if (true === $this->livingRoomPlan->motionOnWithManualOverride([
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
            ])) {

//            if (now()->format('H:i') >= '17:00' && now()->format('H:i') <= '22:00') {
//                $this->deviceMethods->switchAction([
//                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
//                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
//                    DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP'],
//                ], 'On');
//            }

            if (true === $this->livingRoomPlan->onOnce([])) {
                if (now()->isWeekend() || (now()->month == 12 && now()->day >= 25)) {
                    $this->onkyoMethods->turnOn()
                        ->setMainVolume(10)
                        ->setSoundMode('All ch Stereo')
                        ->setSource(OnkyoService::SOURCES['Net']);
                    sleep(1);
                    $this->onkyoMethods->applyOnkyoSetting();
                    $this->onkyoMethods->play();
                }
            }
            return;
        }

        if (true === $this->livingRoomPlan->onWhenDark([
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
            ])) {

            if (now()->format('H:i') >= '17:00' && now()->format('H:i') <= '22:00') {
                $this->deviceMethods->switchAction([
                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                    DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP'],
                ], 'On');
            }

            return;
        }

        if (true === $this->livingRoomPlan->offWhenLight([
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
            ])) {
            return;
        }


//        if (true === $this->livingRoomPlan->motionOn([
//                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
//                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
//                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
//            ])) {
//            return;
//        }

        if (true === $this->livingRoomPlan->doorOpenOn([
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
            ])) {
            return;
        }

        if (true === $this->livingRoomPlan->manualToggle(DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'])) {
            return;
        }

        if (true === $this->livingRoomPlan->manualOn([
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
            ])) {
            return;
        }

        if (true === $this->livingRoomPlan->manualOff([
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
            ])) {
            $this->onkyoMethods->turnOff();
            return;
        }

//        if (true === $this->livingRoomPlan->offAtSunrise([
//                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
//                DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
//                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
//                DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
//            ])) {
//            return;
//        }

        if (true === $this->livingRoomPlan->onOnceAfterSunsetWhenHome([
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM']
            ])) {
            return;
        }

        if (true === $this->livingRoomPlan->offAtTwelveWhenNotHome([
                DomoticzDevicesEnum::DEVICES['LIGHT_LIVING_ROOM'],
            ])) {
            $this->onkyoMethods->turnOff();
            return;
        }


        if (null === $this->idx && $this->deviceState->getCurrentState(DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM']) === 'Off') {
            $this->deviceMethods->switchOffWithInterval(
                DomoticzDevicesEnum::SWITCHES['MOTION_LIVING_ROOM'],
                40,
                [
                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_LAMP'],
                    DomoticzDevicesEnum::DEVICES['WCD_WOODEN_CLOSET'],
                    DomoticzDevicesEnum::DEVICES['WCD_PLANT_LAMP']
                ]);
        }

    }

    private function getHumidity($idx)
    {
        $ch = $this->setupCurl();
        curl_setopt($ch, CURLOPT_URL, sprintf('%s?type=devices&rid=%d', $this->domoticzUrl, $idx));

        $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
        curl_close($ch);

        return $content['result'][0]['Temp'] ?? null;
    }

    /**
     * @throws JsonException
     */
    private function hallwayScripts(): void
    {
        if (($this->idx === DomoticzDevicesEnum::SWITCHES['MOTION_HALLWAY'] && $this->value === 'On') &&
            ($this->deviceMethods->isBetweenSunsetAndSunrise() || $this->deviceMethods->isTwilight())) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['LIGHT_HALLWAY'], 'On');
            return;
        }

        if ($this->deviceState->getCurrentState(DomoticzDevicesEnum::SWITCHES['MOTION_HALLWAY']) === 'Off') {
            $this->deviceMethods->switchOffWithInterval(
                DomoticzDevicesEnum::SWITCHES['MOTION_HALLWAY'],
                2,
                [DomoticzDevicesEnum::DEVICES['LIGHT_HALLWAY']]);
        }
    }

    private function storeState(): void
    {
        if (null !== $this->idx) {
            $deviceLog = new DeviceLog();
            $deviceLog->idx = $this->idx;
            $deviceLog->state = $this->value;
            $deviceLog->save();
        }
    }

    /**
     * @throws JsonException
     */


    /**
     * @throws JsonException
     */
    private function christmasScripts(): void
    {
        $this->christmasOutdoor();

        $this->livingRoomPlan->manualOff(DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_TREE']);
        $this->livingRoomPlan->manualOn(DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_TREE']);

        $now = Carbon::now()->format('H:i');
        if (true === $this->livingRoomPlan->offAtTwelveWhenNotHome([
                DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_TREE']
            ])) {
            return;
        }

        if ($now === '06:00') {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_TREE'], 'On');
        }
    }

    /**
     * @throws JsonException
     */
    private function christmasOutdoor(): void
    {
        $firstDayOfYear = Carbon::now()->startOfYear()->addDays(3);
        $november = clone $firstDayOfYear;
        $november->addMonths(10);
        if (Carbon::now()->isBetween($firstDayOfYear, $november)) {
            return;
        }

        $now = Carbon::now()->format('H:i');
        $sunsetClone = clone $this->sunset;
        if ($now === '06:00' || $now === $sunsetClone->format('H:i')) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_OUTSIDE'], 'On');
        }

        if ($now === '23:59' || $now === $this->sunrise->format('H:i')) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['WCD_CHRISTMAS_OUTSIDE'], 'Off');
        }
    }

    private function hasRelevantDevices(): bool
    {
        $relevantDevices = DomoticzDevicesEnum::DEVICES;
        $relevantDevices = array_merge($relevantDevices, DomoticzDevicesEnum::SWITCHES);

        return null === $this->idx || in_array($this->idx, $relevantDevices, true);
    }

    /**
     * @return void
     * @throws JsonException
     */
    private function outsideScripts(): void
    {
        if (true === $this->livingRoomPlan->onAtSunset([
                DomoticzDevicesEnum::DEVICES['LIGHT_OUTDOOR_1'],
                DomoticzDevicesEnum::DEVICES['LIGHT_OUTDOOR_2']
            ],
                60
            )) {
            return;
        }

        if (true === $this->livingRoomPlan->offAtMidnight([
                DomoticzDevicesEnum::DEVICES['LIGHT_OUTDOOR_1'],
                DomoticzDevicesEnum::DEVICES['LIGHT_OUTDOOR_2']
            ])) {
            return;
        }
    }

    /**
     * @return void
     * @throws JsonException
     */
    private function heatMatScript()
    {
        if ($this->getTemperature(DomoticzDevicesEnum::DEVICES['TEMPERATURE_SENSOR']) <= 20) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['WCD_HEAT_MAT'], 'On');
        } else {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['WCD_HEAT_MAT'], 'Off');
        }
    }

    /**
     * @throws JsonException
     */
    private function showerScript()
    {
        if (true === $this->humidityHasIncreased(DomoticzDevicesEnum::DEVICES['HUMIDITY_SHOWER'], 10)) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['DUCO'], 'On');
        }

        if (true === $this->humidityIsBackToNormal(DomoticzDevicesEnum::DEVICES['HUMIDITY_SHOWER'])) {
            $this->deviceMethods->enableDisableSwitch(DomoticzDevicesEnum::DEVICES['DUCO'], 'Off');
        }
    }

    private function humidityHasIncreased(int $idx, int $increase): bool
    {
        $logs = DeviceLog::query()
            ->where('idx', $idx)
            ->where('created_at', '>', now()->subMinutes(10)->format('Y-m-d H:i:s'))
            ->orderBy('created_at')
            ->limit(2)
            ->get();

        if ($logs->count() === 2 && ($logs[0]->state + $increase) <= $logs[1]->state) {
            return true;
        }

        return false;
    }

    private function humidityIsBackToNormal(int $idx): bool
    {
        $activated = DeviceLog::query()
            ->where('idx', DomoticzDevicesEnum::DEVICES['DUCO'])
            ->where('state', 'On')
            ->latest()
            ->first();

        if (null === $activated) {
            return false;
        }

        $logs = DeviceLog::query()
            ->where('idx', $idx)
            ->where('created_at', '<', $activated->created_at->format('Y-m-d H:i:s'))
            ->orderBy('created_at', 'desc')
            ->first();

        $current = DeviceLog::query()
            ->where('idx', $idx)
            ->latest()
            ->first();

        if (($current->status - 10) <= $logs->status) {
            return true;
        }

        return false;
    }


}
