<?php

namespace App\DataObjects;

use App\AbstractClasses\AutomationVariables;
use App\AbstractClasses\OnkyoMethods;
use Illuminate\Database\Eloquent\Collection;

class LivingRoomDataObject
{


    public $value;
    public $idx;
    public $deviceState;
    public $sunset;
    public $sunrise;
    /**
     * @var Collection
     */
    public $deviceLogs;
    /**
     * @var OnkyoMethods
     */
    public $onkyoMethods;






    public function __construct(AutomationVariables $automationVariables, OnkyoMethods $onkyoMethods)
    {
        $this->idx = $automationVariables->idx;
        $this->value = $automationVariables->value;
        $this->deviceLogs = $automationVariables->deviceLogs;
        $this->deviceState = $automationVariables->deviceState;
        $this->sunset = $automationVariables->sunset;

        $this->onkyoMethods = $onkyoMethods;
    }

}
