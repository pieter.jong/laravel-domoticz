<?php

namespace App\AbstractClasses;

use App\Models\CurrentState;
use App\Services\DomoticzService;

class DeviceState
{

//    use DomoticzMethodsTrait;

    /**
     * @var array
     */
    private $currentDeviceSates;
    /**
     * @var string
     */
    private $domoticzUrl;

    /**
     * @throws \JsonException
     */
    public function __construct(?int $idx, ?string $val)
    {
        $this->domoticzUrl = config('domoticz.url');
        $domoticzService = new DomoticzService();

        if (true === app()->environment('testing')) {
            $this->currentDeviceSates = CurrentState::query()->pluck('state', 'idx');
            $this->currentDeviceSates[$idx] = $val;
        } elseif (null !== $idx) {
//            $this->currentDeviceSates = CurrentState::query()->pluck('state', 'idx');
            $this->currentDeviceSates = $domoticzService->getAllDeviceStates();
            $this->currentDeviceSates[$idx] = $val;
        } else {
            $this->currentDeviceSates = $domoticzService->getAllDeviceStates();
        }
    }

    public function updateState(int $idx, $value)
    {
        $this->currentDeviceSates[$idx] = $value;
    }

    public function getCurrentState(int $idx)
    {
        return $this->currentDeviceSates[$idx] ?? null;
    }

    public function StoreCurrentStateStates()
    {
        CurrentState::truncate();
        $data = collect($this->currentDeviceSates)
            ->map(function ($state, $idx) {
                return ['idx' => $idx, 'state' => $state];
            })
            ->values();


        if ($data->isNotEmpty()) {
            CurrentState::query()->insert($data->toArray());
        }
    }
}
