<?php

namespace App\AbstractClasses;

use App\EnumClasses\DomoticzDevicesEnum;
use App\Services\OnkyoService;
use Exception;
use Illuminate\Support\Facades\Log;

class OnkyoMethods
{

    /**
     * @var OnkyoService
     */
    private $onkyoService;
    /**
     * @var DeviceState
     */
    private $deviceState;

    public function __construct(DeviceState $deviceState)
    {
        $this->onkyoService = new OnkyoService();
        $this->deviceState = $deviceState;
    }

    public function turnOn(): OnkyoMethods
    {
        $this->onkyoService->turnOn();
        return $this;
    }

    public function setMainVolume(int $int): OnkyoMethods
    {
        $this->onkyoService->setMainVolume($int);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function setSoundMode(string $soundMode): OnkyoMethods
    {
        $this->onkyoService->setSoundMode($soundMode);
        return $this;
    }

    public function setSource(string $source): OnkyoMethods
    {
        $this->onkyoService->setSource($source);
        return $this;
    }

    public function play(): OnkyoMethods
    {
        $this->onkyoService->play();
        return $this;
    }

    public function turnOff(): OnkyoMethods
    {
        $this->onkyoService->turnOff();
        return $this;
    }

    public function getInputSource()
    {
        return $this->onkyoService->getInputSource();
    }

    public function onkyoIsOn(): bool
    {
        return $this->onkyoService->onkyoIsOn();
    }

    public function applyOnkyoSetting()
    {
        $state = $this->deviceState->getCurrentState(DomoticzDevicesEnum::SWITCHES['RADIO_STATION_SELECT']);
        Log::info($state);
        switch (true) {
            case $state === 'Set Level: 10 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['3fm']);
                break;
            case $state === 'Set Level: 20 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['NPO radio 2']);
                break;
            case $state === 'Set Level: 30 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Veronica']);
                break;
            case $state === 'Set Level: 40 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Qmusic']);
                break;
            case $state === 'Set Level: 50 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Sky radio']);
                break;
            case $state === 'Set Level: 60 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Freeze fm']);
                break;
            case $state === 'Set Level: 70 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Slam fm']);
                break;
            case $state === 'Set Level: 80 %':
                $this->onkyoService->setRadioPreset(OnkyoService::PRESETS['Kink']);
                break;
        }
    }

    public function changeRadioPreset(?int $idx)
    {
        if (($idx === DomoticzDevicesEnum::SWITCHES['RADIO_STATION_SELECT'] && true === $this->onkyoService->onkyoIsOn())) {
            $this->applyOnkyoSetting();
        }
    }
}
