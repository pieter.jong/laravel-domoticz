<?php

namespace App\AbstractClasses;

use App\Models\DeviceLog;
use Carbon\Carbon;
use JsonException;

class AutomationVariables
{
//    use SunsetSunriseTrait;

    public $switchedDevices = [];
    public $sunset;
    public $sunrise;
    public $deviceState;
    /**
     * @var \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public $deviceLogs;
    /**
     * @var int|null
     */
    public $idx;
    /**
     * @var string|null
     */
    public $value;

    private $lng = 5.821394;
    private $lat = 53.174496;

    /**
     * @throws JsonException
     */
    public function __construct(?int $idx, ?string $value)
    {
        $this->idx = $idx;
        $this->value = $value;
        $date = now()->format('d-m');
        //Summer
        if ($date > '26-03') {
            $this->sunset = $this->sunset()->addMinutes(15);
            $this->sunrise = $this->sunrise();
        }

        //Winter
        if ($date > '15-09' || $date < '25-03') {
            $this->sunset = $this->sunset()->subMinutes(30);
            $this->sunrise = $this->sunrise()->addMinutes(30);
        }
        $this->deviceState = new DeviceState($idx, $value);

        $deviceLogs = DeviceLog::query()
            ->where('created_at', '>', now()->startOfDay()->format('y-m-d H:i:s'))
            ->get();
        $this->deviceLogs = $deviceLogs; //todo deviceLogs moet een object worden want deze wordt op meerder plaatsen aangesproken. Op deze manier hebben we niet synchrone data.
    }

    private function sunrise(Carbon $onDay = null): Carbon
    {
        $onDay = $onDay ?? Carbon::now();

        $sunriseTimestamp = date_sun_info(
            (int)$onDay->timestamp,
            $this->lat,
            $this->lng
        )['sunrise'];

        return Carbon::createFromTimestamp($sunriseTimestamp);
    }

    private function sunset(Carbon $onDay = null): Carbon
    {
        $onDay = $onDay ?? Carbon::now();

        $sunsetTimestamp = date_sun_info(
            (int)$onDay->timestamp,
            $this->lat,
            $this->lng
        )['sunset'];

        return Carbon::createFromTimestamp($sunsetTimestamp);
    }
}
