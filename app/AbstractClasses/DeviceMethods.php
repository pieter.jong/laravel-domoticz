<?php

namespace App\AbstractClasses;

use App\EnumClasses\DomoticzDevicesEnum;
use App\Models\DeviceLog;
use App\Services\DomoticzService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use JsonException;

class DeviceMethods
{
    private $deviceState;
    /**
     * @var AutomationVariables
     */
    private $automationVariables;
    /**
     * @var DomoticzService
     */
    private $domoticzService;
    private $sunset;
    private $sunrise;
    private $idx;
    private $deviceLogs;

    public function __construct(AutomationVariables $automationVariables)
    {
        $this->deviceState = $automationVariables->deviceState;
        $this->automationVariables = $automationVariables;
        $this->domoticzService = new DomoticzService();
        $this->sunset = $automationVariables->sunset;
        $this->sunrise = $automationVariables->sunrise;
        $this->idx = $automationVariables->idx;
        $this->deviceLogs = $automationVariables->deviceLogs;
    }

    public function switchAction($devices, string $state): void
    {
        if (is_array($devices)) {
            foreach ($devices as $device) {

                if ($this->deviceState->getCurrentState($device) === $state) {
                    continue;
                }
                if (in_array($device, $this->automationVariables->switchedDevices)) {
                    sleep(1);
                }
                $this->deviceState->updateState($device, $state);
                $this->deviceLogs[] = new DeviceLog([
                    'idx' => $device,
                    'state' => $state,
                    'created_at' => now(),
                ]);
                $this->automationVariables->switchedDevices[] = $device;
                if (false === app()->environment('testing')) {
                    $this->enableDisableSwitch($device, $state);
                }

            }
        } else {
            if ($this->deviceState->getCurrentState($devices) === $state) {
                return;
            }
            if (in_array($devices, $this->automationVariables->switchedDevices)) {
                sleep(1);
            }
            $this->deviceState->updateState($devices, $state);
            $this->deviceLogs[] = new DeviceLog([
                'idx' => $devices,
                'state' => $state,
                'created_at' => now(),
            ]);
            $this->automationVariables->switchedDevices[] = $devices;

            if (false === app()->environment('testing')) {
                $this->enableDisableSwitch($devices, $state);
            }
        }
    }

    /**
     * @throws JsonException
     */
    public function enableDisableSwitch(int $idx, string $command)
    {
        $this->domoticzService->enableDisableSwitch($idx, $command);

    }

    public function switchOffWithInterval(int $dependencyDevice, int $minutes, array $switchDevices): void
    {
        if (null === $this->idx) {
            $interval = Carbon::now()->subMinutes($minutes)->format('Y-m-d H:i:s');
            $maySwitchOff = DeviceLog::query()
                ->where('idx', $dependencyDevice)
                ->where('created_at', '>', $interval)
                ->count();

            if ($maySwitchOff === 0) {
                Log::notice('Switch off with interval');
                foreach ($switchDevices as $device) {
                    $this->enableDisableSwitch($device, 'Off');
                }
            }
        }
    }

    public function isBetweenSunsetAndSunrise(): bool
    {
        $now = Carbon::now();
        return $now >= $this->sunset || $now < $this->sunrise;
    }

    public function isTwilight(): bool
    {
        $log = $this->deviceLogs
            ->where('idx', DomoticzDevicesEnum::DEVICES['LUX_LIVING_ROOM'])
            ->sortByDesc('created_at')
            ->first();

        if (!$log) {
            return false;
        }

        $time = now();

        // Morning twilight check
        if ($time->between(
            $time->copy()->setTime(5, 0),
            $time->copy()->setTime(9, 0)
        )) {
            return $log->state <= 17;
        }

        // Evening twilight check
        if ($time->between(
            $time->copy()->setTime(16, 0),
            $time->copy()->setTime(18, 0)
        )) {
            return $log->state <= 20;
        }

        return false;
    }

    /**
     * @throws JsonException
     */
    public function setDimLevel($idx, string $dimLevel)
    {
        $this->domoticzService->setDimLevel($idx, $dimLevel);
    }

}
