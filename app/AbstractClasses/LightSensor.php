<?php

namespace App\AbstractClasses;

use App\Models\DeviceLog;
use Illuminate\Support\Collection;

class LightSensor
{
    /**
     * @var Collection
     */
    private $deviceLogCollection;

    /**
     * Filtered collection after applying conditions.
     *
     * @var Collection
     */
    private $filteredCollection;
    /**
     * @var bool
     */
    private $hasNoState;

    /**
     * @var Closure|null
     */
    private $filterCallback = null;

    public function __construct(int $idx, Collection $deviceLogCollection)
    {
        // Filter the initial collection to match the provided idx
        $this->deviceLogCollection = $deviceLogCollection->where('idx', $idx);
        $this->filteredCollection = $this->deviceLogCollection;
        $this->hasNoState = $this->filteredCollection->isEmpty() ? true : false;
    }
    public function darkerThen(int $lux)
    {
        // Store a different filtering logic in a traditional anonymous function
        $this->filterCallback = function ($log) use ($lux) {
            return $log->state < $lux;
        };

        return $this;
    }

    public function within(int $minutes): self
    {
        $thresholdTime = now()->subMinutes($minutes);

        $lastLog = $this->filteredCollection
            ->filter(function (DeviceLog $log) use ($thresholdTime) {
                return $log->created_at > $thresholdTime;
            })->last();

        $this->filteredCollection = collect($lastLog ? [$lastLog] : []);

        return $this;
    }

    public function lighterThen(int $lux): self
    {
        // Store the filtering logic in a traditional anonymous function
        $this->filterCallback = function ($log) use ($lux) {
            return $log->state > $lux;
        };

        return $this;
    }


    public function check(): ?bool
    {
        if ($this->hasNoState) {
            return null; // TODO: Perform status check in Domoticz and add to DeviceState
        }

        // Apply the stored callback if it exists
        if ($this->filterCallback) {
            $this->filteredCollection = $this->filteredCollection->filter($this->filterCallback);
        }

        return $this->filteredCollection->isNotEmpty();
    }


}
