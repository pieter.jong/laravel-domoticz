<?php


namespace App\Http\Controllers;


use App\Services\OnkyoService;

class DashboardController extends Controller
{
    private $domoticzUrl = 'http://192.168.1.8:8080/json.htm?';


    /**
     * @var OnkyoService
     */
    private $onkyoService;

    public function __construct(OnkyoService $onkyoService)
    {
        $this->onkyoService = $onkyoService;
    }

    public function onkyoRelatedThings()
    {
        $test = $this->onkyoService->getMainVolume();
        dd($test);
        $this->onkyoService->setSoundMode('Dolby surround');
        $this->onkyoService->setMainVolume(25);
        $this->onkyoService->setCenterVolume(5);
    }

    //Dit is even een test script. hiermee zou je hee makkelijk allemaal lampen kunnen in of uit schakelen
    public function index()
    {
        $usedRIds = [];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//        $result = $this->enableDisableSwitch(19, 'Off');

        $iterations = 200;
        for ($i = 1; $i < $iterations; $i++) {
            curl_setopt($ch, CURLOPT_URL, sprintf('%stype=devices&rid=%d', $this->domoticzUrl, $i));
            $content = json_decode(curl_exec($ch), true, 512, JSON_THROW_ON_ERROR);
            if (isset($content['result'])) {
                $values = $content['result'][0];
                $usedRIds[$i]['all'] = $values;
                $usedRIds[$i . $values['Name']]['joe'] = $values['Name'];
//                $usedRIds[$i . $values['result][[0'['Namestatus'] = $values['Status'];
                $usedRIds[$i . $values['Name']]['idx'] = $values['idx'];
                $usedRIds[$i . $values['Name']]['type'] = $values['Type']; //Categorie
//                $usedRIds[$i . $values['switchType'] = $values['SwitchType'];
            }
        }

        curl_close($ch);
        dd($values);

//        dd($usedRIds);

        return view('dashboard.index');
    }

    /**
     * @throws \JsonException
     */
    private function enableDisableSwitch(int $idx, string $cmd): string
    {
        $reply = json_decode(
            file_get_contents($this->domoticzUrl . 'type=command&param=switchlight&idx=' . $idx . '&switchcmd=' . $cmd),
            true, 512, JSON_THROW_ON_ERROR
        );
        return $reply['status'] === 'OK' ? 'OK' : $reply = 'ERROR';
    }

    /**
     * @throws \JsonException
     */
    private function enableDisableScene($idx, $cmd)
    {
        $reply = json_decode(
            file_get_contents($this->domoticzUrl . 'type=command&param=switchscene&idx=' . $idx . '&switchcmd=' . $cmd),
            true, 512, JSON_THROW_ON_ERROR
        );
        return $reply['status'] === 'OK' ? 'OK' : 'ERROR';
    }

    /**
     * @throws \JsonException
     */
    private function setDimLevel($idx, $level)
    {
        if ($level > 0 && $level < 100) {
            ++$level;
        }
        $reply = json_decode(
            file_get_contents($this->domoticzUrl . 'type=command&param=switchlight&idx=' . $idx . '=&switchcmd=Set%20Level&level=' . $level),
            true, 512, JSON_THROW_ON_ERROR
        );
        return $reply['status'] === 'OK' ? 'OK' : 'ERROR';
    }





}
