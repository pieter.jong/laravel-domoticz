<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Vonq API requirements
    |--------------------------------------------------------------------------
    |
    | These tokens are required to make a connection to the vonq API.
    | For testing, other credentials should be used, that can be obtained by vonq
    */

    'username' => env('DOMOTICZ_API_USERNAME', null),
    'password' => env('DOMOTICZ_API_PASSWORD', null),
    'url' => env('DOMOTICZ_API_URL', null),
];
